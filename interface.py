import tkinter as tk

root = tk.Tk()
root.title("Project Interface")


def run_project1():
    import os
    import cv2

    DATA_DIR = 'C:/Users/User/PycharmProjects/pythonProject1/data'
    if not os.path.exists(DATA_DIR):
        os.makedirs(DATA_DIR)

    number_of_classes = 3
    dataset_size = 100

    cap = cv2.VideoCapture(0)
    for j in range(number_of_classes):
        if not os.path.exists(os.path.join(DATA_DIR, str(j))):
            os.makedirs(os.path.join(DATA_DIR, str(j)))

        print('Collecting data for class {}'.format(j))

        done = False
        while True:
            ret, frame = cap.read()
            cv2.putText(frame, 'Press "R" when ready', (100, 50), cv2.FONT_HERSHEY_SIMPLEX, 1.3, (0, 255, 0), 3,
                        cv2.LINE_AA)
            cv2.imshow('frame', frame)
            if cv2.waitKey(25) == ord('r'):
                break

        counter = 0
        while counter < dataset_size:
            ret, frame = cap.read()
            cv2.imshow('frame', frame)
            cv2.waitKey(25)
            cv2.imwrite(os.path.join(DATA_DIR, str(j), '{}.jpg'.format(counter)), frame)

            counter += 1

    cap.release()
    cv2.destroyAllWindows()


def run_project2():
    import os
    import pickle
    import mediapipe as mp
    import cv2
    #import matplotlib.pyplot as plt

    mp_hands = mp.solutions.hands
    mp_drawing = mp.solutions.drawing_utils
    mp_drawing_styles = mp.solutions.drawing_styles

    hands = mp_hands.Hands(static_image_mode=True, min_detection_confidence=0.3)

    DATA_DIR = 'C:/Users/User/PycharmProjects/pythonProject1/data'

    data = []
    labels = []
    for dir_ in os.listdir(DATA_DIR):
        for img_path in os.listdir(os.path.join(DATA_DIR, dir_)):
            data_aux = []

            x_ = []
            y_ = []

            img = cv2.imread(os.path.join(DATA_DIR, dir_, img_path))
            img_rgb = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

            results = hands.process(img_rgb)
            if results.multi_hand_landmarks:
                for hand_landmarks in results.multi_hand_landmarks:
                    for i in range(len(hand_landmarks.landmark)):
                        x = hand_landmarks.landmark[i].x
                        y = hand_landmarks.landmark[i].y

                        x_.append(x)
                        y_.append(y)

                    for i in range(len(hand_landmarks.landmark)):
                        x = hand_landmarks.landmark[i].x
                        y = hand_landmarks.landmark[i].y
                        data_aux.append(x - min(x_))
                        data_aux.append(y - min(y_))

                data.append(data_aux)
                labels.append(dir_)

    f = open('data.pickle', 'wb')
    pickle.dump({'data': data, 'labels': labels}, f)
    f.close()


def run_project3():
    import pickle

    from sklearn.ensemble import RandomForestClassifier
    from sklearn.model_selection import train_test_split
    from sklearn.metrics import accuracy_score
    import numpy as np

    data_dict = pickle.load(open('./data.pickle', 'rb'))

    data = np.asarray(data_dict['data'])
    labels = np.asarray(data_dict['labels'])

    x_train, x_test, y_train, y_test = train_test_split(data, labels, test_size=0.2, shuffle=True, stratify=labels)

    model = RandomForestClassifier()

    model.fit(x_train, y_train)

    y_predict = model.predict(x_test)

    score = accuracy_score(y_predict, y_test)

    print('{}% of samples were classified correctly !'.format(score * 100))

    f = open('model.p', 'wb')
    pickle.dump({'model': model}, f)
    f.close()


def run_project4():
    import pickle
    import cv2
    import mediapipe as mp
    import numpy as np

    model_dict = pickle.load(open('./model.p', 'rb'))
    model = model_dict['model']

    cap = cv2.VideoCapture(0)

    mp_hands = mp.solutions.hands
    mp_drawing = mp.solutions.drawing_utils
    mp_drawing_styles = mp.solutions.drawing_styles

    hands = mp_hands.Hands(static_image_mode=True, min_detection_confidence=0.3)

    labels_dict = {0: 'E', 1: 'D', 2: 'I'}
    while True:

        data_aux = []
        x_ = []
        y_ = []

        ret, frame = cap.read()

        H, W, _ = frame.shape

        frame_rgb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)

        results = hands.process(frame_rgb)
        if results.multi_hand_landmarks:
            for hand_landmarks in results.multi_hand_landmarks:
                mp_drawing.draw_landmarks(
                    frame,  # image to draw
                    hand_landmarks,  # model output
                    mp_hands.HAND_CONNECTIONS,  # hand connections
                    mp_drawing_styles.get_default_hand_landmarks_style(),
                    mp_drawing_styles.get_default_hand_connections_style())

            for hand_landmarks in results.multi_hand_landmarks:
                for i in range(len(hand_landmarks.landmark)):
                    x = hand_landmarks.landmark[i].x
                    y = hand_landmarks.landmark[i].y

                    x_.append(x)
                    y_.append(y)

                for i in range(len(hand_landmarks.landmark)):
                    x = hand_landmarks.landmark[i].x
                    y = hand_landmarks.landmark[i].y
                    data_aux.append(x - min(x_))
                    data_aux.append(y - min(y_))

            x1 = int(min(x_) * W) - 10
            y1 = int(min(y_) * H) - 10

            x2 = int(max(x_) * W) - 10
            y2 = int(max(y_) * H) - 10

            prediction = model.predict([np.asarray(data_aux)])

            predicted_character = labels_dict[int(prediction[0])]

            cv2.rectangle(frame, (x1, y1), (x2, y2), (0, 0, 0), 4)
            cv2.putText(frame, predicted_character, (x1, y1 - 10), cv2.FONT_HERSHEY_SIMPLEX, 1.3, (0, 0, 0), 3,
                        cv2.LINE_AA)

        cv2.imshow('frame', frame)
        cv2.waitKey(1)

    cap.release()
    cv2.destroyAllWindows()

button1 = tk.Button(root, text="Project 1", command=run_project1)
button2 = tk.Button(root, text="Project 2", command=run_project2)
button3 = tk.Button(root, text="Project 3", command=run_project3)
button4 = tk.Button(root, text="Project 4", command=run_project4)

root.configure(bg="#F0F0F0")

title_label = tk.Label(root, text="Hand Sign Recognition System", font=("Arial", 20, "bold"), bg="#F0F0F0")
title_label.pack(pady=20)

frame1 = tk.Frame(root, bg="#FFFFFF", padx=20, pady=10)
frame1.pack(pady=10)

instructions_label = tk.Label(root, text="Please run the code using the buttons in numerical order.", font=("Arial", 14), bg="#F0F0F0")
instructions_label.pack()

frame1 = tk.Frame(root, bg="#FFFFFF", padx=20, pady=10)
frame1.pack(pady=10)

button1 = tk.Button(frame1, text="1. Collect the Samples", font=("Arial", 14), bg="#6495ED", padx=10, pady=5, command=run_project1)
button1.pack(side=tk.LEFT, padx=10)

button2 = tk.Button(frame1, text="2. Build the Database", font=("Arial", 14), bg="#C41E3A", padx=10, pady=5, command=run_project2)
button2.pack(side=tk.LEFT, padx=10)

frame2 = tk.Frame(root, bg="#FFFFFF", padx=20, pady=10)
frame2.pack(pady=10)

button3 = tk.Button(frame2, text="3. Train the Program", font=("Arial", 14), bg="#50C878", padx=10, pady=5, command=run_project3)
button3.pack(side=tk.LEFT, padx=10)

button4 = tk.Button(frame2, text="4. Test the Program", font=("Arial", 14), bg="#E49B0F", padx=10, pady=5, command=run_project4)
button4.pack(side=tk.LEFT, padx=10)



root.mainloop()
