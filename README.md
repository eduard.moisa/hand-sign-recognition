# HAND SIGN RECOGNITION SYSTEM

Am creat un proiect de recunoastere a literelor din limbajul semnelor american. Pentru implementarea acestui proiect trebuie ca prima oara sa avem un mediu de programare python pe care sa ruleze acest cod. Eu am ales Pycharm. Acestuia ii vom configura ca interpretor Python 3.9. Deasemenea avem nevoie de o camera web, integrata a laptopului sau una externa.


# Instalare bibliotecilor necesare

Trebuie instalate urmatoarele biblioteci in acest mediu de programare:

-   OpenCV: pip install opencv-python
-   mediapipe: pip install mediapipe
-   scikit-learn: pip install scikit-learn
-   tkinter (ar trebui sa fie inclusa cu orice instalare python) daca nu, folosim sudo apt-get install python3-tk


# Rulare si testare program 

Cream un nou proiect in care vom copia codul atasat

In continuare va trebui sa modificam directorul in care se salveaza cadrele video astfel incat sa se potriveasca cu setup-ul pe care se testeaza aplicatia. 

- DATA_DIR = 'C:/Users/User/PycharmProjects/pythonProject1/data'

Deasemenea in functie de tipul de camera folosit va trebui sa ne asiguram ca parametrul cv2 este corect. 

- cap = cv2.VideoCapture(0) 

Default acest parametru este 0 (zero). Alte valori pe care le poate lua acest paramatru sunt 1 sau 2, daca aveti de exemplu 2 camere web. 

Apoi vom putea rula aplicatia utilizand butonul de RUN din dreapta sus.

Folosim interfata pentru a testa aplicatia astfel:

- Button 1 pentru a captura video semnele din limbajul semnelor american. Semnele se pot observa in imaginea atasata.
- Button 2 pentru a creea baza de date
- Button 3 pentru a antrena programul
- Button 4 pentru a testa sistemul de recunoastere

# LINKUL CATRE PROIECT https://gitlab.upt.ro/eduard.moisa/hand-sign-recognition
